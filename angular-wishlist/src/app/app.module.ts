import { BrowserModule } from '@angular/platform-browser';
import { NgModule , InjectionToken, APP_INITIALIZER, Injectable} from '@angular/core';
import { RouterModule , Routes } from '@angular/router';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { StoreModule as NgRxStoreModule, Store } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { HttpClientModule, HttpRequest, HttpHeaders,HttpClient } from '@angular/common/http';
import Dexie from 'dexie';
import { TranslateService, TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 

import { AppComponent } from './app.component';
import { CrucerosViajeComponent } from './components/cruceros-viaje/cruceros-viaje.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import { FormDestinoCruceroComponent } from './components/form-destino-crucero/form-destino-crucero.component';
import { CruceroviajeState, reducerCruceroViajes,intializeCruceroViajeState, CruceroViajeEffects, InitMyDataAction } from './models/crucero-viajes-states.model';
import { ActionReducerMap } from '@ngrx/store';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { AuthService } from './services/auth.service';
import { VuelosComponentComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { VuelosMainComponentComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosDetalleComponentComponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { ReservasModule } from './reservas/reservas.module';
import { from,Observable } from 'rxjs';
import { DestinoCrucero } from './models/destinoViaje.model';
import { flatMap } from 'rxjs/operators';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';


//app config
export interface AppConfig  { //interfaz app config
  apiEndpoint: String;
}

const APP_CONFIG_VALUE: AppConfig= {
  apiEndpoint: 'http://localhost:3000'
}

export const APP_CONFIG   = new InjectionToken<AppConfig>('app.config');

//carga el conj. de rutas principales
//RUTA HIJAS
export const childrenRoutesVuelos :Routes =[ //rutas hijas de vuelos,conjunto de ruta adicional
  { path:'', redirectTo:'main', pathMatch:'full'},
  { path:'main' ,component: VuelosMainComponentComponent},
  { path:'mas-info' ,component: VuelosMasInfoComponentComponent},
  { path:':id' , component: VuelosDetalleComponentComponent}
]

//ruta RAIZ
const routes: Routes= [//ruta definidas
  {path: '', redirectTo:'inicio', pathMatch:'full'},
  {path: 'inicio', component: ListaDestinosComponent},
  {path:'destino/:id', component: DestinoDetalleComponent},
  {path:'login' , component:LoginComponent},
  {path:'protected', component:ProtectedComponent, canActivate:[UsuarioLogueadoGuard]},
  {path:'vuelos', component: VuelosComponentComponent, canActivate:[UsuarioLogueadoGuard], 
  children:childrenRoutesVuelos}
];

//redux init
//define un estado global de la app
export interface AppState {
  destino: CruceroviajeState;

}
//reducers globales de la app
const reducers: ActionReducerMap<AppState> = {
  destino: reducerCruceroViajes
};

const reducersInitialState  = {//sincronico
  destino: intializeCruceroViajeState()
};
//redux fin init

//app init
export function init_app(appLoadService: AppLoadService): () => Promise<any> {
  return () => appLoadService.intializeCruceroViajeState();//hace ciertas tareas cuando se inicia
}

@Injectable()
class AppLoadService  {
  constructor(private store: Store<AppState>, private http: HttpClient){}
  async intializeCruceroViajeState(): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN':'token-seguridad'});
    const req = new HttpRequest('GET',APP_CONFIG_VALUE.apiEndpoint + '/my', { headers: headers });
    const response: any = await this.http.request(req).toPromise();//devuelve una promesa
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}

//dexie db
export class Translation {//guarda local con dexie
  constructor(public id: number, public lang: string, public key: string, public value: string){}
}

@Injectable({
  providedIn: 'root'
})
//actualizamos 
export class MyDatabase extends Dexie {
  destino: Dexie.Table<DestinoCrucero, number>;
  translations: Dexie.Table<Translation, number>;
  constructor(){
    //versionado de bd
    super('MyDatabase');
    this.version(1).stores({//guarda STORES
      destino: '++id, nombre, imagenUrl'
    });
    this.version(2).stores({//guarda destinos y translations
      //por si entra meses despues, muestra coomo evoluciono la bd, se adapta a lo que llame al esquema de la bd
      destino: '++id, nombre, imagenUrl',
      translations:'++id, lang, key, value'
    });
  }
}

export const db = new MyDatabase();


//i18n ini  cargador costume
class TranslationLoader implements TranslateLoader{ //classe de traducciones
  constructor(private http: HttpClient){}
  
  getTranslation(lang: string): Observable<any> { //metodo que retorna un observable
    const promise = db.translations //vamos a db y hacemos la consulta
                      .where('lang')
                      .equals(lang)
                      .toArray()
                      .then(results => { //callback 
                        if(results.length === 0){//cuando no esta en la bd
                          return this.http//si no esta en la bd local , no da respuesta 
                            .get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang='+ lang)
                            .toPromise()
                            .then(apiResults => {
                              db.translations.bulkAdd(apiResults);//devuelve el api la respuesta y lo agrego a la traduccion
                              //retorno 
                              return apiResults;
                            });
                        }
                        return results;//aqui termina el callback
                      }).then((traducciones) => { //cuando se carga la traduccion
                        console.log('traducciones cargadas:');
                        console.log(traducciones);
                        return traducciones;
                      }).then((traducciones) => {//mapea a como espera ngxtraslater
                        return traducciones.map((t) => ({ [t.key]: t.value}));
                      });
                      //flatmap=> array de arrays con un unico elemento
    return from(promise).pipe(flatMap((elems) => from(elems)));//devuelve un array de valores 
  }
}

function HttpLoaderFactory(http: HttpClient) {
  return new TranslationLoader(http);
}



@NgModule({//agrega metadatos a la clase inmediatamente siguiente
  //constructor
  declarations: [//claves declaraciones; especifica los componentes que definen en el modulo
    AppComponent,
    CrucerosViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoCruceroComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentComponent,
    VuelosMainComponentComponent,
    VuelosMasInfoComponentComponent,
    VuelosDetalleComponentComponent,
    EspiameDirective,
    TrackearClickDirective,
        
  ],
  imports: [ //importaciones; Deascriben dependencias del modulo
    BrowserModule,
    FormsModule,//
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes), //decorador del modulo 
    NgRxStoreModule.forRoot(reducers, { initialState : reducersInitialState }),
    EffectsModule.forRoot([CruceroViajeEffects]) ,
    StoreDevtoolsModule.instrument(),
    ReservasModule, // pude ser parametrizado
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    NgxMapboxGLModule,
    BrowserAnimationsModule
  ],
  providers: [
    AuthService,
    UsuarioLogueadoGuard,
    {provide: APP_CONFIG,useValue:APP_CONFIG_VALUE},
    AppLoadService, 
    {provide: APP_INITIALIZER, useFactory:init_app, deps:[AppLoadService], multi:true},
    MyDatabase
    //UserService 
  ],//proveedores; inyeccion de dependencia
  bootstrap: [AppComponent] //bootstrap
})
export class AppModule { }
