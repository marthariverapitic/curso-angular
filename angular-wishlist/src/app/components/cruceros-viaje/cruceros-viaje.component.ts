import { Component, OnInit, Input, HostBinding ,EventEmitter, Output} from '@angular/core';//idenifica a un nuevo decorador el input
import { DestinoCrucero } from './../../models/destinoViaje.model';
import { Store } from '@ngrx/store';
import { AppState } from './../../app.module';
import { voteUpAction, voteDownAction } from './../../models/crucero-viajes-states.model';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-cruceros-viaje',
  templateUrl: './cruceros-viaje.component.html',
  styleUrls: ['./cruceros-viaje.component.css'],
  animations: [//atributo, que recibe un array de animaciones , de triggers
    trigger('esFavorito', [//aniimacion
      state('estadoFavorito', style({//cuando el obj se marca como favorito cambia el background
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor:'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito',[
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito',[
        animate('1s')
      ])
    ])
  ]
})
export class CrucerosViajeComponent implements OnInit {
  //agregar variable del nombre/titulo
  @Input() destino: DestinoCrucero;//pasa a parametro al tag de viaje de plantillas
  @Input('idx') position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4'; //se hace un tag ficticio 
  @Output() clicked: EventEmitter<DestinoCrucero>; //el tipo en el generic


  constructor(private store: Store<AppState>) {
    //inicializamos var
    this.clicked= new EventEmitter();//cargamos el obj para usarla
    
   }

  ngOnInit() {
    
  }
  //metodo ir
  ir(){
    this.clicked.emit(this.destino);//atributo para eventos el clicked y que destino se clickeo
    return false;
  }

  voteUp() {
    this.store.dispatch(new voteUpAction(this.destino)); //le digo al store que me lo despache
    return false;
  }

  voteDown() {
    this.store.dispatch(new voteDownAction(this.destino));
    return false;
  }

}
