import { Component, OnInit} from '@angular/core';
import { DestinoCrucero } from './../../models/destinoViaje.model';
import { ActivatedRoute } from '@angular/router';
import { destinosApiClient } from './../../models/destinos-api-client.model';


@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [
    destinosApiClient
  ]// cuando se borra del appmodules se agrega como proveedor

})
export class DestinoDetalleComponent implements OnInit {
  destino: DestinoCrucero;

  style = {//atributo , hace referencia del html
    sources: {//origenes de datos
      world: {
        type:'geojson',
        data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'//vinculamos la info de un json de paises a nivel mundial
      }
    },
    version:8,
    layers: [{
      'id': 'countries',
      'type': 'fill',
      'source': 'world',
      'layout': {},
      'paint': {
        'fill-color': '#6F788A'
      }
    }]
  };
  constructor(private route: ActivatedRoute, private DestinoApiClient: destinosApiClient) {
   
   }

   ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.destino = this.DestinoApiClient.getById(id);
    
  }
  
}
