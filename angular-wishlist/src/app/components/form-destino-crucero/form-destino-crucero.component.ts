import { Component, OnInit, Output, EventEmitter, Inject, forwardRef } from '@angular/core';
import { DestinoCrucero } from './../../models/destinoViaje.model';
import { FormBuilder, FormGroup, Validators, FormControl, Validator, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map,filter,debounceTime,distinctUntilChanged, switchMap} from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { APP_CONFIG, AppConfig } from 'src/app/app.module';

@Component({
  selector: 'app-form-destino-crucero',
  templateUrl: './form-destino-crucero.component.html',
  styleUrls: ['./form-destino-crucero.component.css']
})
export class FormDestinoCruceroComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoCrucero>;//evento 
  fg:FormGroup;//variable de el formgroup
  minLongitud= 3;
  searchResults: string[];

  //forwardRef > inyeccion de dependencias  para hacer declaraciones (referencia circular uno carga al otro)
  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config :AppConfig) {//dependencias del formbuilder ,permite definir el formulario
    this.onItemAdded = new EventEmitter();//inicializamos el output
    
    this.fg = fb.group({//inicializamos el formgroup
      nombre:['',Validators.compose([//array de validador
        Validators.required,
        this.nombreValidator,
        this.nombreValidatorParametrizable(this.minLongitud)
      ])],//validador
      url:['']//objt de inicializacion, que controles vincularemos, para vincular al html
    });
    this.fg.valueChanges.subscribe((form: any)=>{
      console.log('cambio el forumulario'+ form);
    });
  }

  ngOnInit() {
    //detectar en la caja de texto nombre, levantar el texto y usarlo para usar sugerencias
    const elemNombre= <HTMLInputElement>document.getElementById('nombre');
    //escucha cada vez que escribe una tecla el usuario en la caja de texto
    //recibe un callback
    fromEvent(elemNombre, 'input')  //genera un observable de eventos de entrada
    //flujo secuenciales en serie
    .pipe(
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value), //es un string de eventos del teclado 
      filter(text => text.length > 3 ),//entrada al siguiente operador
      debounceTime(200),//se queda en stop y si llegan nuevos tecleos actualiza el tecleo
      distinctUntilChanged(), //si llegan distintos valores del elemento despues del borrado  no llega lo borrado
      //se hace ajax al endpoint
      switchMap((text: string) => ajax(this.config.apiEndpoint + '/ciudades?q='+ text))//api <de busqueda
    ).subscribe(AjaxResponse => this.searchResults = AjaxResponse.response);
  }

  guardar(nombre: string, url: string): boolean {
    const d = new DestinoCrucero(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }
 //validador
  nombreValidator(control:FormControl): { [s:string]:boolean } {//obj qe devuelve booelano
    const l  = control.value.toString().trim().length;//recibimos el valor 
    if(l > 0 && l < 5){//logica para validar 
      return {invalidNombre:true};//retorna objeto
    }
    return null;
  }

  //validador parametrizable 
  nombreValidatorParametrizable(minLong:number): ValidatorFn{
    return (control:FormControl) : {[s:string]:boolean} | null  => {
      const l  = control.value.toString().trim().length;//recibimos el valor 
      
      if(l > 0 && l < minLong){//logica para validar 
        return {minLongNombre:true};//retorna objeto
      }
      return null;
    }
  }
}
