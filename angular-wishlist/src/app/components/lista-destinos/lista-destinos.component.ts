import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { DestinoCrucero } from './../../models/destinoViaje.model';
import { destinosApiClient } from './../../models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from './../../app.module';
import { ElegidoFavoritoAction, NuevoCruceroAction } from './../../models/crucero-viajes-states.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [ destinosApiClient ]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoCrucero>;
  updates : string[];//aqui se esta actualizando
  all;
  // destino:DestinoCrucero[]; 
  constructor(private DestinosApiClient: destinosApiClient, private store: Store<AppState>) { 
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    //suscribe montar una parte del estado
    this.store.select(state => state.destino.favorito) //solo importa las actualizaciones de favorito del destino
      .subscribe(d => {// subscribe de cambios
        if(d != null){//lo que ha cambiado de favorito dentro de destino y state global
          console.log(d.nombre);
          this.updates.push('se ha elegido a : '+ d.nombre);
        }
      });
      //asigna cambio a los items en el state del store asigna los nuevos items a la variable
      store.select(state => state.destino.items).subscribe(items => this.all = items);

  }

  ngOnInit() {
  }
  //viene del boton del html
  agregado(d: DestinoCrucero) {
    this.DestinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }
  elegido(e:DestinoCrucero){//destino
    this.DestinosApiClient.elegir(e); //ya se hizo una funcion en el models
  }

  getAll() {

  }
}
