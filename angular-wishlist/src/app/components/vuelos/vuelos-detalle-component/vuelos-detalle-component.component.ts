import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-vuelos-detalle-component',
  templateUrl: './vuelos-detalle-component.component.html',
  styleUrls: ['./vuelos-detalle-component.component.css']
})
export class VuelosDetalleComponentComponent implements OnInit {
  id: any;
  constructor(private route: ActivatedRoute) { //recibe el objeto relacion a la ruta de la url
    route.params.subscribe(params => {this.id = params['id']; //el params id sale de la conf. de la ruta
    });
  }

  ngOnInit() {
  }

}
