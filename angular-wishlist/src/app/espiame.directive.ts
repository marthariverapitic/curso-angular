import { Directive,OnInit,OnDestroy } from '@angular/core';

@Directive({
  selector: '[appEspiame]'
})
export class EspiameDirective  implements OnInit, OnDestroy {//implementa oninit interfaz que nos ayuda a implementar una interfaz
  static nextId = 0;
  log =(msg: string) => console.log(`Evento #${++EspiameDirective.nextId} ${msg}` );//permite hacer una interpolacion de cadenas
  ngOnInit(){
    this.log('#####******* OnInit');
  }

  ngOnDestroy(){//loguear en la consola, delegamos en un meto log
    this.log('#####******* onDestroy');
  }

  constructor() { }

}
