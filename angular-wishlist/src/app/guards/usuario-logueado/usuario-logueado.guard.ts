import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot,  CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class UsuarioLogueadoGuard implements  CanActivate { //canactivate interfaz que implementa el metodo de la lin 12
  constructor(private authService: AuthService){}

  //deacuerdo a la ruta y el siguiente elemento en funcion de los paarametros analiza si se activa el guard
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot ): Observable<boolean> | Promise<boolean> | boolean{
    const isLoggedIn = this.authService.isLoggedIn(); //si se loguea se activa
    console.log("CanActive", isLoggedIn);
    return isLoggedIn;
  }
  
}
