import {reducerCruceroViajes, 
        CruceroviajeState,
        intializeCruceroViajeState, 
        InitMyDataAction,
        NuevoCruceroAction} from './crucero-viajes-states.model';
import { DestinoCrucero } from './destinoViaje.model';


describe('reducerCruceroViajes', () => {
    //tests
    it('should reducer init data', () => {
        //setup=> cuando uno arma los objetos que uno va a necesitar para test
        const prevState: CruceroviajeState = intializeCruceroViajeState();
        const action: InitMyDataAction = new InitMyDataAction(['destino 1' ,'destino 2']);
        //action sobre el codigo=>
        const newState: CruceroviajeState = reducerCruceroViajes(prevState,action);//solicita al codigo que realice una accion
        //assertions => cuando el valor retornado del codigo(verificaciones)
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual('destino 1');
        //tear down => ejecutar el codigo para borrar loq ue se inserto en la bd , en caso que haya conexion al a bd
    });

    it('should reducer new item added', () => {
        const prevState: CruceroviajeState = intializeCruceroViajeState();
        const action: NuevoCruceroAction = new NuevoCruceroAction(new DestinoCrucero('cancun','url'));
        const newState: CruceroviajeState = reducerCruceroViajes(prevState,action);//estimula el dominioo
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].nombre).toEqual('cancun');
    });
});