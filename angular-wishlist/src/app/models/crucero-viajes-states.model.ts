import { Injectable } from '@angular/core';
import { Action, StateObservable } from '@ngrx/store';
import { Actions,Effect, ofType } from '@ngrx/effects';
import { Observable, of}  from 'rxjs';
import { map } from 'rxjs/operators';
import { DestinoCrucero } from './destinoViaje.model';
import { HttpClientModule } from '@angular/common/http';


// ESTADO global de la app, interfaz
export interface CruceroviajeState {
    items: DestinoCrucero[];
    loading: boolean;//para tirar el concepto para variable de un ajax syncronica,,llamadasss a servicio remoto
    favorito: DestinoCrucero;
}
//asincronico
export function intializeCruceroViajeState() { //inicializamos el estado ,manejo del estado
    return {
        items:[],
        loading: false,
        favorito: null
    };
};


// ACCIONES que modifican el estado, usuario interactuando
export enum CruceroViajesActionTypes {
    NUEVO_DESTINO       = '[Destino Crucero] Nuevo',
    ELEGIDO_FAVORITO    = '[Destino Crucero] Favorito',
    VOTE_UP             = '[Destino Crucero] Vote Up',
    VOTE_DOWN           = '[Destino Crucero] Vote Down',
    INIT_MY_DATA        = '[Destino Crucero] Init My Data'
}
//clases que representan a la accion
export class NuevoCruceroAction implements Action { 
    type = CruceroViajesActionTypes.NUEVO_DESTINO;
    constructor (public destino: DestinoCrucero){}
}
export class voteUpAction implements Action { 
    type = CruceroViajesActionTypes.VOTE_UP;
    constructor (public destino: DestinoCrucero){}
}
export class voteDownAction implements Action { 
    type = CruceroViajesActionTypes.VOTE_DOWN;
    constructor (public destino: DestinoCrucero){}
}

export class ElegidoFavoritoAction implements Action {
    type = CruceroViajesActionTypes.ELEGIDO_FAVORITO;
    constructor(public destino: DestinoCrucero) {}
}
  
export class InitMyDataAction implements Action {
    type = CruceroViajesActionTypes.INIT_MY_DATA;
    constructor(public destino: string[]) {} //se pone a string por que donde recibe, recibe un string
}


//agrupa los tipos de datos de las acciones, union de tipos
export type CruceroViajesActions = NuevoCruceroAction | ElegidoFavoritoAction | voteUpAction | voteDownAction | InitMyDataAction;

//reducers (reductores) cuando se dispara una accion llama de 1 a 1 , cada areducer recibe el estado anterior
//del sistema y recibe la accion que dispara en funcion de un swtich
export function reducerCruceroViajes(
    state: CruceroviajeState,
    action: CruceroViajesActions
): CruceroviajeState {
    switch (action.type) {
        case CruceroViajesActionTypes.INIT_MY_DATA: {
            const destino: string[] = (action as InitMyDataAction).destino; //se castea y accede a los destinos
            return {
                ...state,
                items: destino.map((d) => new DestinoCrucero(d, '')) //altera el estado, inicializa el items
            }
        }
        case CruceroViajesActionTypes.NUEVO_DESTINO: {
            return {
                ...state,
                items:[...state.items, (action as NuevoCruceroAction).destino]
            };
        }
        case CruceroViajesActionTypes.ELEGIDO_FAVORITO: {
            state.items.forEach(x => x.setSelected(false));
            const fav: DestinoCrucero = (action as ElegidoFavoritoAction).destino;
            fav.setSelected(true);
            return{
                ...state,
                favorito:fav
            };
        }
        case CruceroViajesActionTypes.VOTE_UP: {
            const d: DestinoCrucero = (action as voteUpAction).destino;
            d.voteUp();
            return{ ...state};//clona el estado
        }
        case CruceroViajesActionTypes.VOTE_DOWN: {
            const d: DestinoCrucero = (action as voteDownAction).destino;
            d.voteDown();
            return{ ...state};//clona el estado
        }
    }
    return state; //devuelve el lestado sin mutar
}

//effects , despues que se dispara la accion, los reducers  despues que muta el estado
// esa accion se pasa a los effects registrados en sistema, registra una nueva accion 
//como consecuencia de otra accion
@Injectable()
export class CruceroViajeEffects {
    @Effect()
    nuevoAgregado$ : Observable<Action> = this.actions$.pipe(
        ofType(CruceroViajesActionTypes.NUEVO_DESTINO),//filtra el tipo de accion que nos interesa
        map((action: NuevoCruceroAction) => new ElegidoFavoritoAction(action.destino))//elige como nuevo favorito
    );
    constructor(private actions$: Actions){}
}