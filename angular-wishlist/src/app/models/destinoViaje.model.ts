import { v4 as uuid } from 'uuid';

export class DestinoCrucero{
    
    private selected:boolean;//bandera 
    public servicios: string[];
    id = uuid();
    
    constructor(public  nombre :string,  public u:string, public votes:number = 0){
        
        this.servicios= ['Desayuno Continental','barra libre'];//servicios al seleccionar en el formulario
    }//declara var publicas en constructor
    
    isSelected() {
        return this.selected;
    }
    setSelected(s:boolean){//encapsula el acceso a la variable
        this.selected=s;
    }

    voteUp() {
        this.votes++;
    }

    voteDown() {
        this.votes--;
    }
    
}


