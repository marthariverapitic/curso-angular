import { Injectable, Inject, forwardRef } from '@angular/core';
import { DestinoCrucero } from './destinoViaje.model';
import { Store } from '@ngrx/store';
import { AppState, APP_CONFIG, AppConfig, db } from './../app.module';
import { ElegidoFavoritoAction, NuevoCruceroAction } from '../models/crucero-viajes-states.model';
import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http'; 

@Injectable()
export class destinosApiClient {
	destino: DestinoCrucero[] = [];
	
	//
	constructor( private store: Store<AppState>, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig, 
	private http:HttpClient) {
		this.store
		.select(state => state.destino)
		.subscribe((data) => {
			console.log("destinos sub store");
			console.log(data);
			this.destino = data.items;
		});
		this.store
		.subscribe((data) => {
			console.log("all store");
			console.log(data);
		})
	   	
	}


	add(d:DestinoCrucero){
		const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN':'token-seguridad'});//para ver como se agrega el encabezado y se envia en el post
		const req = new HttpRequest('POST' , this.config.apiEndpoint + '/my' , {nuevo:d.nombre},{headers : headers});
		this.http.request(req).subscribe((data: HttpResponse<{}>) => {
			if(data.status=== 200){//codigo 200 que el servidor respondio bien, se proceso correctamente
				this.store.dispatch(new NuevoCruceroAction(d));//despacha laa accion del nuevo destino
				const myDb = db;
				myDb.destino.add(d);//segagramos la info  a la bd
				console.log("todos los cruceros de la db!");
				myDb.destino.toArray().then(destino => console.log(destino));
			}
		})
	}

	getAll(): DestinoCrucero[] {
		return this.destino;
	}
	
		//decimos cual es el elegido
	elegir(d:DestinoCrucero){
		this.store.dispatch(new ElegidoFavoritoAction(d));
	}

	getById(id:String): DestinoCrucero {
		return this.destino.filter(function(d){
			return d.id.toString() === id;
		})[0];

	}
} 