import { Injectable } from '@angular/core';

@Injectable({ //servicio inyectable
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  login( user: string, password: string): boolean{
    if(user === 'user'  && password  === 'password'){
      localStorage.setItem('username',user); //objeto que guarda valores en el navegador
      return true;
    }
    return false;
  }

  logout(): any{
     localStorage.removeItem('username');
  }

  getUser(): any{ //levanta el valor del setItem
    return localStorage.getItem('username');
  }

  isLoggedIn(): boolean{
    return this.getUser() !== null;
  }
}
