import { Directive, ElementRef } from '@angular/core';
import { fromEvent } from 'rxjs';

@Directive({
  selector: '[appTrackearClick]'
})
export class TrackearClickDirective {
  private element: HTMLInputElement;

  constructor(private elRef: ElementRef) {
    this.element = elRef.nativeElement;
    //subscrivo al click del elemento, cada vez que se hace el click se hace el callback track
    fromEvent(this.element, 'click').subscribe(evento => this.track(evento));
   }

   track(evento: Event): void {//cuando ocurre un evento se ve si tiene un atributo data-trackear-tacks y se separa por espacios en blanco
     const elemTags = this.element.attributes.getNamedItem('data-trackear-tags').value.split(' ');
     console.log(`|||||| track evento: "${elemTags}"`);
   }

}
