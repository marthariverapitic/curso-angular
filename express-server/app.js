var express = require("express"), cors = require('cors');
var app = express();//inicializa servidor web
app.use(express.json()); //indicamos que usaremos json
app.use(cors());
app.listen(3000, () => console.log("Servidor puerto 3000")); //que estara ene l puerto 3000

var ciudades=["Cancun","Mazatlan","san Carlos","Puerto Peñasco","Mazamitla"];
app.get("/ciudades", (req, res, next) => res.json(ciudades.filter((c) => c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1)));

var misDestinos =[];
app.get("/my", (req,res,next) => res.json(misDestinos));
app.post("/my", (req,res, next) => {
    console.log(req.body);
    misDestinos.push(req.body.nuevo); //se agrego el req body nuevo 
    res.json(misDestinos);
})
//se piden las traducciones nos pasa como argumento el lenguaje
app.get("/api/translation",(req,res,next) => res.json([//archivo con todas las traducciones
    //por cada texzto a traducir son 3 var de lenguaje
    //key texto a traducir
    //value valor traducido del string para ese lenguaje
    {lang: req.query.lang, key: 'HOLA', value:'HOLA ' + req.query.lang}//retorna un hola y valor traducido de hola
]))